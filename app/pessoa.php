<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class pessoa extends Model
{

    protected $table = "pessoa";

    protected $fillable = [ 'nome', 'cpf', 'email', 'dt_nascimento','celular', 'endereco_id'];

    public function endereco(){
        return $this->hasMany(endereco::class);
    }
}
