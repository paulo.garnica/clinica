<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;


class UsuarioController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function __construct()
    {
        $this->middleware('auth');
    }


    public function index()
    {
        if(!checkPermission(['admin','superadmin']))
        {
            return redirect()->route('home');
        }
    
        $User = User::where('is_permission','!=', '2')->paginate(5);
  
        return view('Usuarios.userList',compact('User'))->with('i', (request()->input('page', 1) - 1) * 5);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        if(!checkPermission(['admin','superadmin']))
        {
            return redirect()->route('home');
        }
       
        return view('Usuarios.create');

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if(!checkPermission(['admin','superadmin']))
        {
            return redirect()->route('home');
        }

        $request->validate([
            'name' => 'required',
            'email' => 'required',
            'password' => 'required|min:6|required_with:password_confirmation|same:password_confirmation',
            'password_confirmation' => 'required'
        ]);

     
  
        User::create([
            'name' => $request->input('name'),
            'email' => $request->input('email'),
            'password' => Hash::make($request->input('password')),
            'is_permission' => $request->input('is_permission')
        ]);
   
        return redirect("usuario")->with('success','Usuário criado com sucesso.');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\User  $user
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        if(!checkPermission(['admin','superadmin']))
        {
            return redirect()->route('home');
        }
        $User = User::find($id);
        return view('Usuarios.show',compact('User'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\User  $User
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        if(!checkPermission(['admin','superadmin']))
        {
            return redirect()->route('home');
        }
        $User = User::find($id);
        return view('Usuarios.edit',compact('User'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\User  $User
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        if($request->input('password') == "")
        {
            DB::table('users')
            ->where('id', $id)
            ->update([
            'id' => $id,
            'name' => $request->input('name'),
            'email' => $request->input('email'),
            'is_permission' => $request->input('is_permission')
        ]); 
        }else{
      
        DB::table('users')
            ->where('id', $id)
            ->update([
            'id' => $id,
            'name' => $request->input('name'),
            'email' => $request->input('email'),
            'password' => Hash::make($request->input('password')),
            'is_permission' => $request->input('is_permission')
        ]);
    }
        return redirect('usuario')->with('success','Usuário atualizado com sucesso');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\User  $User
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $User = User::find($id);
        $User -> delete();
        return redirect('usuario')->with('success','Usuário removido com sucesso');
    }

}
