<?php

namespace App\Http\Controllers;

use App\pacients;
use App\endereco;
use App\pessoa;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;


class PacientsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function __construct()
    {
        $this->middleware('auth');
    }


    public function index()
    {
        $pessoas = pessoa::latest()->paginate(5);
  
        error_log($pessoas);
        return view('pacientes.index',compact('pessoas'))
            ->with('i', (request()->input('page', 1) - 1) * 5);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('pacientes.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'nome' => 'required',
            'cpf' => 'required',
            'email' => 'required',
            'dt_nascimento' => 'required',
            'celular' => 'required',
            'cep' => 'required',
            'logradouro' => 'required',
            'numero' => 'required',
            'bairro' => 'required',
            'cidade' => 'required',
            'estado' => 'required',
            
        ]);
  
        $endereco = new endereco;
        
        error_log(empty($request->complemento));
         
        $endereco->cep =  $request->cep;
        $endereco->logradouro =  $request->logradouro;
        $endereco->complemento =  empty($request->complemento) == 1 ? null : $request->complemento;
        $endereco->numero =  $request->numero;
        $endereco->bairro =  $request->bairro;
        $endereco->cidade =  $request->cidade;
        $endereco->estado =  $request->estado;   
        $endereco->pais =  'Brasil';   

        $endereco -> save();

        $pessoa = new pessoa;

        $pessoa->nome= $request->nome;
        $pessoa->cpf= $request->cpf;
        $pessoa->email= $request->email;
        $pessoa->dt_nascimento= $request->dt_nascimento;
        $pessoa->celular= $request->celular;
        $pessoa->endereco_id= $endereco->id;

        $pessoa -> save();

        return redirect()->route('pacientes.index')
                        ->with('success','Paciente adicionado com sucesso!');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\pacients  $pacients
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $pessoa = pessoa::find($id);
        $endereco = endereco::find($pessoa->endereco_id);
        
        return view('pacientes.show',compact('pessoa','endereco'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\pacients  $pacients
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $pessoa = pessoa::find($id);
        $endereco = endereco::find($pessoa->endereco_id);
        error_log($pessoa);
        return view('pacientes.edit',compact('pessoa','endereco'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\pacients  $pacients
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'nome' => 'required',
            'cpf' => 'required',
            'email' => 'required',
            'dt_nascimento' => 'required',
            'celular' => 'required',
            'cep' => 'required',
            'logradouro' => 'required',
            'numero' => 'required',
            'bairro' => 'required',
            'cidade' => 'required',
            'estado' => 'required',
            
        ]);
  
        $pessoa = pessoa::find($id);
        $endereco = endereco::find($pessoa->endereco_id);

        DB::table('pessoa')
        ->where('id', $id)
        ->update([
        'nome' => $request->input('nome'),
        'cpf' => $request->input('cpf'),
        'email' => $request->input('email'),
        'dt_nascimento' => $request->input('dt_nascimento'),
        'celular' => $request->input('celular')]);

        DB::table('endereco')
        ->where('id', $pessoa->endereco_id)
        ->update([
        'cep' => $request->input('cep'),
        'logradouro' => $request->input('logradouro'),
        'complemento' => $request->input('complemento'),
        'numero' => $request->input('numero'),
        'bairro' => $request->input('bairro'),
        'cidade' => $request->input('cidade'),
        'estado' => $request->input('estado')
        ]);
      
        return redirect()->route('pacientes.index')
                        ->with('success','Paciente editado com sucesso!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\pacients  $pacients
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $pessoa = pessoa::find($id);
        $endereco = endereco::find($pessoa->endereco_id);
        
        $pessoa -> delete();
        return redirect()->route('pacientes.index')
                        ->with('success','Paciente removido com sucesso!');
    }
 
}
