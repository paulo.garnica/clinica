<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class pacients extends Model
{
    protected $fillable = [ 'nome', 'cpf', 'dt_nasc', 'email','cep', 'logradouro', 'numero', 'bairro', 'cidade','estado'];
}
