<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class endereco extends Model
{
    protected $table = "endereco";

    protected $fillable = [ 'cep', 'logradouro', 'numero', 'complemento','bairro', 'cidade', 'estado', 'pais'];
}
