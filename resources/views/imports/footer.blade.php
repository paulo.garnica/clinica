

<footer class="footer-light" id="5">
  <div class="container" style="min-height: 250px !important;"> 
    <div class="row">
      <div class="col-sm-3">
        <div class="heading-footer">
          <h4>Quem Somos</h4>
        </div>
        <p>Clínica Humaitá, sempre cuidando de você!</p>
      </div>
      <div class="col-sm-3 mg25-xs">
        <div class="heading-footer" style="margin:0;">
          <h4>Redes Sociais</h4>
        </div>
        <a href="https://www.facebook.com/clinicahumaita/" target="_blank" style="font-size: 50pt;">
          <i class="fab fa-facebook-square"></i>
        </a>
      </div>
      <div class="col-sm-3 mg25-xs">
        <div class="heading-footer">
          <h4>Contato</h4>
        </div>
        <p><span class="ion-home footer-info-icons"></span><small class="address">Rua Humaita, 1494</small></p>
        <p><span class="ion-ios7-telephone footer-info-icons"></span><small class="address">(14) 4103-0818</small></p>
        <p><span class="ion-ios7-telephone footer-info-icons"></span><small class="address">(14) 99185-0184</small></p>
      </div>
    </div>
    <div class="row">
      <hr>
      <div class="col-sm-11 col-xs-10">
        <p class="copyright">© <span id="ano"></span> Clínica Humaitá. Todos os direitos reservados.</p>
      </div>
      <div class="col-sm-1 col-xs-2 text-right">
        <a href="#top-top" class="scroll-down scroll">
          <div class="footer-scrolltop-holder">
              <span class="ion-ios7-arrow-up footer-scrolltop mt-2 arrow" id="2"></span>
          </div>
      </a>
      </div>
    </div>
  </div>
</footer>



<script type="text/javascript">

  document.getElementById("ano").innerHTML = new Date().getFullYear(); 

  $(".scroll").click(function (event) {
      event.preventDefault();
      $('html,body').animate({scrollTop:$(this.hash).offset().top},1000);
  });
  
  </script>