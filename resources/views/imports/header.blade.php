
 

<!--Navbar-->
<nav class="navbar navbar-expand-lg navbar-light fadeInUp-animated" id="fixed-navbar">
  <!-- Navbar brand -->
  <a class="navbar-brand scroll" href="#top-top"><div class="css-logo"></div> Clínica Humaitá</a>
  <!-- Collapse button -->
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#basicExampleNav"
    aria-controls="basicExampleNav" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>
  <!-- Collapsible content -->
  <div class="collapse navbar-collapse" id="basicExampleNav">
    <!-- Links -->
    <ul class="navbar-nav mr-auto">
      <li class="nav-item active">
        <a class="nav-link scroll-down scroll" href="#1">Início</a>
      </li>
      <li class="nav-item">
        <a class="nav-link scroll-down scroll" href="#2">Quem somos</a>
      </li>
      <li class="nav-item">
        <a class="nav-link scroll-down scroll" href="#3">Localização</a>
      </li>
      <li class="nav-item">
        <a class="nav-link scroll-down scroll" href="#4">Contato</a>
      </li>
      <li class="nav-item m-auto">
        <a class="nav-link" href="/login">Login</a>
      </li>
    </ul>
    <!-- Links -->
 </div>
  <!-- Collapsible content -->
</nav>
<!--/.Navbar-->

