@extends('layout.app')
@section('content')
<div class="container mt-5">

    <form action="{{ route('usuario.update',$User->id) }}" method="POST">
        @csrf
        @method('PUT')
        <div class="card">
            <div class="card-header text-center">{{ __('Editar Usuário') }}</div>
            <div class="card-body">
                <div class="form-group row">
                    <label for="name" class="col-md-4 col-form-label text-md-right">{{ __('Nome') }}</label>

                    <div class="col-md-6">
                        <input id="name" type="text" class="form-control @error('name') is-invalid @enderror"
                            name="name" value="{{$User->name}}" required autocomplete="name" autofocus>

                        @error('name')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                        @enderror
                    </div>
                </div>

                <div class="form-group row">
                    <label for="email" class="col-md-4 col-form-label text-md-right">{{ __('E-Mail') }}</label>

                    <div class="col-md-6">
                        <input id="email" type="email" class="form-control @error('email') is-invalid @enderror"
                            name="email" value="{{$User->email }}" required autocomplete="email">

                        @error('email')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                        @enderror
                    </div>
                </div>
                <div class="form-group row">
                    <label for="password" class="col-md-4 col-form-label text-md-right">{{ __('Senha') }}</label>

                    <div class="col-md-6">
                        <input id="password" type="password"
                            class="form-control @error('password') is-invalid @enderror" name="password"
                            autocomplete="new-password">

                        @error('password')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                        @enderror
                    </div>
                </div>
                <div class="form-group row">
                    <label for="password-confirm"
                        class="col-md-4 col-form-label text-md-right">{{ __('Confirmação da senha') }}</label>

                    <div class="col-md-6">
                        <input id="password-confirm" type="password" class="form-control" name="password_confirmation"
                            autocomplete="new-password">
                    </div>
                </div>
                <div class="form-group row">
                    <label for="is_permission" class="col-md-4 col-form-label text-md-right">Permissão</label>

                    <div class="col-md-6 hidden">
                        <select id="is_permission" name="is_permission" class="form-control">
                            @if($User->is_permission == 1)
                            <option value="1" selected>Administrador</option>
                            @else
                            <option value="1">Administrador</option>
                            @endif

                            @if($User->is_permission != 1)
                            <option value="3" selected>Usuário</option>
                            @else
                            <option value="3">Usuário</option>
                            @endif
                        </select>
                    </div>
                </div>
                <div class="form-group row mb-0">
                    <div class="col-md-3 offset-md-4">
                        <button type="submit" class="btn btn-success">
                            {{ __('Salvar') }}
                        </button>
                    </div>
                    <div class="col-md-2">
                        <a class="btn btn-primary" href="{{ route('usuario.index') }}"> Voltar</a>
                    </div>
                </div>
            </div>
        </div>
    </form>
</div>
@endsection