@extends('layout.app')
@section('content')
<div class="container mt-5">


    <div class="card">
        <div class="card-header text-center">{{ __('Detalhes do Usuário') }}</div>

        <div class="card-body">


            <div class="form-group row">
                <label for="name" class="col-md-4 col-form-label text-md-right">Nome</label>

                <div class="col-md-6">
                    <input id="name" type="text" class="form-control " name="name" value="{{$User->name}}" disabled>

                </div>
            </div>

            <div class="form-group row">
                <label for="email" class="col-md-4 col-form-label text-md-right">E-Mail</label>

                <div class="col-md-6">
                    <input id="email" type="email" class="form-control " name="email" value="{{$User->email}}" disabled>

                </div>
            </div>




            <div class="form-group row">
                <label for="is_permission" class="col-md-4 col-form-label text-md-right">Permissão</label>

                <div class="col-md-6 hidden">
                    <input id="is_permission" type="text" class="form-control " name="is_permission" value="@switch($User->is_permission)
                        @case(1)
Administrador
                        @break
        
                        @case(2)
Super Administrador
                        @break
        
                        @default
Usuário Padrão
                        @endswitch" disabled>

                </div>
            </div>
            <div class="form-group row mb-0">
                <div class="col-md-6 offset-md-4">
                    <a class="btn btn-primary" href="{{ route('usuario.index') }}"> Voltar</a>
                </div>
            </div>
            
        </div>
        
    </div>

     
   
</div>
@endsection