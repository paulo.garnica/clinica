@extends('layout.app')

@section('content')
<div class="container mt-4" style="margin-bottom:20vh;">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header text-center">{{ __('Criar novo usuário') }}</div>

                <div class="card-body">
                    <form method="POST" action="{{ route('usuario.store') }}">
                        @csrf

                        <div class="form-group row">
                            <label for="name" class="col-md-4 col-form-label text-md-right">{{ __('Nome') }}</label>

                            <div class="col-md-6">
                                <input id="name" type="text" class="form-control @error('name') is-invalid @enderror"
                                    name="name" value="{{ old('name') }}" required autocomplete="name" autofocus>

                                @error('name')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="email" class="col-md-4 col-form-label text-md-right">{{ __('E-Mail') }}</label>

                            <div class="col-md-6">
                                <input id="email" type="email" class="form-control @error('email') is-invalid @enderror"
                                    name="email" value="{{ old('email') }}" required autocomplete="email">

                                @error('email')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="password"
                                class="col-md-4 col-form-label text-md-right">{{ __('Senha') }}</label>

                            <div class="col-md-6">
                                <input id="password" type="password"
                                    class="form-control @error('password') is-invalid @enderror" name="password"
                                    required autocomplete="new-password">

                                @error('password')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="password-confirm"
                                class="col-md-4 col-form-label text-md-right">{{ __('Confirmação da senha') }}</label>

                            <div class="col-md-6">
                                <input id="password-confirm" type="password" class="form-control"
                                    name="password_confirmation" required autocomplete="new-password">
                            </div>
                        </div>

                        @if(checkPermission(['admin','superadmin']))
                        <div class="form-group row">
                            <label for="is_permission"
                                class="col-md-4 col-form-label text-md-right">{{ __('Permissão') }}</label>

                            <div class="col-md-6 hidden">
                                <select id="is_permission" name="is_permission" class="form-control">
                                    <option value="1">Administrador</option>
                                    <option value="3" selected>Usuário</option>

                                </select>

                                @error('is_permission')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                                @enderror
                            </div>
                        </div>
                        @else
                        <div class="form-group row" style="display:none;">
                            <label for="is_permission"
                                class="col-md-4 col-form-label text-md-right">{{ __('Permission') }}</label>

                            <div class="col-md-6 hidden">
                                <input id="is_permission" type="text"
                                    class="form-control @error('is_permission') is-invalid @enderror"
                                    name="is_permission" value="0" required>

                                @error('is_permission')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                                @enderror
                            </div>
                        </div>
                        @endif

                        <div class="form-group row mb-0">
                            <div class="col-md-4 offset-md-3">
                                <button type="submit" class="btn btn-success">
                                    {{ __('Criar usuário') }}
                                </button>
                            </div>
                            <div class="col-md-4">
                                <a class="btn btn-primary" href="{{ route('usuario.index') }}"> Voltar</a>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection