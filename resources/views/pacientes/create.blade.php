@extends('layout.app')

@section('content')

<div class="container mt-5">
    <div class="card">
        <div class="card-header text-center">{{ __('Adicionar Pacientes') }}</div>
        <div class="card-body">
            <div class="row">

            </div>

            @if ($errors->any())
            <div class="alert alert-danger">
                <strong>Whoops!</strong> Algum problema com os dados informados.<br><br>
                <ul>
                    @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
            @endif

            <form action="{{ route('pacientes.store') }}" method="POST">
                @csrf

                <div class="row">
                    <div class="col-xs-12 col-sm-12 col-md-12">
                        <div class="form-group">
                            <strong>Nome Completo:</strong>
                            <input type="text" id="nome" name="nome" required class="form-control" placeholder="Nome Completo">
                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-12 col-md-6">
                        <div class="form-group">
                            <strong>CPF:</strong>
                            <input type="text" id="cpf" name="cpf" required class="form-control" placeholder="CPF">
                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-12 col-md-6">
                        <div class="form-group">
                            <strong>Data Nascimento:</strong>
                            <input type="date" id="dt_nascimento" name="dt_nascimento" required class="form-control"
                                placeholder="Data de nascimento">
                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-12 col-md-6">
                        <div class="form-group">
                            <strong>E-mail:</strong>
                            <input type="text" id="email" name="email" required class="form-control" placeholder="E-Mail">
                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-12 col-md-6">
                        <div class="form-group">
                            <strong>Celular:</strong>
                            <input type="text" id="celular" name="celular" required class="form-control" placeholder="Celular">
                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-12 col-md-6">
                        <div class="form-group">
                            <strong>CEP:</strong>
                            <input type="text" id="cep" name="cep" required class="form-control" placeholder="CEP" onblur="pesquisacep(this.value)">
                        </div>
                    </div>
                    <div class="col-md-1"></div>
                    <div class="col-xs-12 col-sm-12 col-md-6">
                        <div class="form-group">
                            <strong>Logradouro:</strong>
                            <input type="text" id="logradouro" name="logradouro" required class="form-control" placeholder="Logradouro">
                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-12 col-md-2">
                        <div class="form-group">
                            <strong>Número:</strong>
                            <input type="text" id="numero" name="numero" required class="form-control" placeholder="Número">
                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-12 col-md-4">
                        <div class="form-group">
                            <strong>Complemento:</strong>
                            <input type="text" id="complemento" name="complemento" class="form-control" placeholder="Complemento">
                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-12 col-md-6">
                        <div class="form-group">
                            <strong>Bairro:</strong>
                            <input type="text" id="bairro" name="bairro" required class="form-control" placeholder="Bairro">
                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-12 col-md-6">
                        <div class="form-group">
                            <strong>Cidade:</strong>
                            <input type="text" name="cidade" id="cidade" required class="form-control" placeholder="Cidade">
                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-12 col-md-6">
                        <div class="form-group">
                            <strong>Estado:</strong>
                            <input type="text" name="estado" id="estado" required class="form-control" placeholder="Estado">
                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-12 col-md-6">
                        <div class="form-group">
                            <strong>País:</strong>
                            <input type="text" name="pais" id="pais" required class="form-control" value="Brasil" disabled>
                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-12 col-md-6">
                        <div class="form-group">
                            <button type="submit" class="btn btn-success">Salvar</button>
                            <a class="btn btn-primary" href="{{ route('pacientes.index') }}"> Voltar</a>
                        </div>
                    </div>
                </div>
                
               
               
            </form>
        </div>
    </div>
</div>

<script type="text/javascript">
             $(document).ready(function () {
                 
                 $('#cpf').mask('999.999.999-99');
                 $('#celular').mask('(99)99999-9999');
                 $('#cep').mask('99999-999');
                return false;
            });


            function pesquisacep(valor)
            {

                valor = valor.replace("-", "");
                //Nova variável "cep" somente com dígitos.
                var cep = valor.replace(/\D/g, '');

                //Verifica se campo cep possui valor informado.
                if (cep != "")
                {
                    //Expressão regular para validar o CEP.
                    var validacep = /^[0-9]{8}$/;

                    //Valida o formato do CEP.
                    if (validacep.test(cep))
                    {
                        //Cria um elemento javascript.
                        var script = document.createElement('script');
                        //Sincroniza com o callback.
                        script.src = 'https://viacep.com.br/ws/' + cep + '/json/?callback=meu_callback';
                        //Insere script no documento e carrega o conteúdo.
                        document.body.appendChild(script);
                    } //end if.
                    else
                    {
                        //cep é inválido.
                        limpa_formulário_cep();
                        alert("Formato de CEP inválido.");
                    }
                } //end if.
                else
                {
                    //cep sem valor, limpa formulário.
                    limpa_formulário_cep();
                }
            }

            function meu_callback(conteudo)
            {
                if (!("erro" in conteudo)) {
                    
                    //Atualiza os campos com os valores.
                    document.getElementsByName('logradouro')[0].value = (conteudo.logradouro);
                    document.getElementsByName('bairro')[0].value = (conteudo.bairro);
                    document.getElementsByName('cidade')[0].value = (conteudo.localidade);
                    document.getElementById("complemento").value = (conteudo.complemento);
                    document.getElementsByName('estado')[0].value = (conteudo.uf);
                  
                }  
                else
                {
                    limpa_formulário_cep();
                    alert("CEP não encontrado.");
                }
            }

            function limpa_formulário_cep()
            {
               document.getElementById("cep").value = "";
               document.getElementById("logradouro").value = "";
               document.getElementById("numero").value = "";
               document.getElementById("bairro").value = "";
               document.getElementById("complemento").value = "";
               document.getElementById("cidade").value = "";
               
            }

</script>

@endsection