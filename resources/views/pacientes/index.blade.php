@extends('layout.app')
 
@section('content')
<div class="container mt-5">
    <div class="card">
        <div class="card-header text-center">{{ __('Pacientes') }}</div>
        <div class="card-body">
    <div class="row">
        <div class="col-lg-12 margin-tb">
           
            <div class="pull-right  mt-4 mb-4">
                <a class="btn btn-success" href="{{ route('pacientes.create') }}"> Novo Paciente</a>
            </div>
        </div>
    </div>
   
    @if ($message = Session::get('success'))
        <div class="alert alert-success">
            <p>{{ $message }}</p>
        </div>
    @endif
    @if(count($pessoas) == 0)
            <div class="row">
                <div class="col-md-12 hscroll text-center">
                    <span>Não foram encontrados registros.</span>
                </div>
            </div>
            
            @elseif (count($pessoas) > 0)
    <table class="table table-bordered">
        <tr>
            <th>Nome</th>
            <th>E-Mail</th>
            <th>CPF</th>
            <th>Celular</th>
            <th>Ações</th>
        </tr>
        @foreach ($pessoas ?? '' as $pessoa)
        <tr>
            <td>{{ $pessoa->nome }}</td>
            <td>{{ $pessoa->email }}</td>
            <td>{{ $pessoa->cpf }}</td>
            <td>{{ $pessoa->celular }}</td>
            <td>
                <form id="form{{$pessoa->id}}" action="{{ route('pacientes.destroy',$pessoa->id) }}" method="POST">
                    @csrf
                    @method('DELETE')
      
                    
                    <a class="btn btn-info" href="{{ route('pacientes.show',$pessoa->id) }}">Detalhes</a>
                    <a class="btn btn-primary" href="{{ route('pacientes.edit',$pessoa->id) }}">Editar</a>
                    @if(checkPermission(['admin','superadmin']))

                    <a href="javascript:;" data-toggle="modal" onclick="deleteData({{$pessoa->id}})" data-target="#DeleteModal" class="btn btn-xs btn-danger">Remover</a>
                     
                    @endif
                </form>
            </td>
        </tr>
        @endforeach
    </table>
    <div id="DeleteModal" class="modal fade text-danger" role="dialog">
        <div class="modal-dialog ">
          <!-- Modal content-->
          <form action="{{ route('pacientes.destroy',$pessoa->id) }}" method="POST">
            @csrf
            @method('DELETE')

              <div class="modal-content">
                  <div class="modal-header bg-danger">
                      <!-- <button type="button" class="close" data-dismiss="modal">&times;</button> -->
                      <h4 class="modal-title text-center" style="color:white;">Remover paciente</h4>
                  </div>
                  <div class="modal-body">
               
                      <p class="text-center">Tem certeza que deseja remover o paciente <span id="nomeusuario" hidden></span> ?</p>
                  </div>
                  <div class="modal-footer text-center">
                      
                          <button type="button" class="btn btn-success" data-dismiss="modal">Cancelar</button>
                          <button type="button" class="btn btn-danger" onclick="submitform()">Remover</button>
                       
                  </div>
              </div>
          </form>
        </div>
       </div>
    </div>
    @endif
        
       
    {!! $pessoas ?? ''->links() !!}
</div> 
<script type="text/javascript">
    function deleteData(id)
    {
        $("#nomeusuario").html(id)
    }
    function submitform(id)
    {
        $("#form"+$("#nomeusuario").html()).submit();
    }
 </script>
@endsection