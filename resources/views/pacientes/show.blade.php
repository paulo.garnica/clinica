@extends('layout.app')

@section('content')

<div class="container mt-5">
    <div class="card">
        <div class="card-header text-center">{{ __('Detalhes do Pacientes') }}</div>
        <div class="card-body">
            <div class="row">
                <div class="col-xs-12 col-sm-12 col-md-12">
                    <div class="form-group">
                        <strong>Nome Completo:</strong>
                        <input type="text" id="nome" name="nome" class="form-control" disabled value="{{ $pessoa->nome }}">
                    </div>
                </div>
                <div class="col-xs-12 col-sm-12 col-md-6">
                    <div class="form-group">
                        <strong>CPF:</strong>
                        <input type="text" id="cpf" name="cpf" required class="form-control" disabled value="{{ $pessoa->cpf }}">
                    </div>
                </div>
                <div class="col-xs-12 col-sm-12 col-md-6">
                    <div class="form-group">
                        <strong>Data Nascimento:</strong>
                        <input type="date" id="dt_nascimento" name="dt_nascimento" required class="form-control" disabled value="{{ $pessoa->dt_nascimento }}">
                    </div>
                </div>
                <div class="col-xs-12 col-sm-12 col-md-6">
                    <div class="form-group">
                        <strong>E-mail:</strong>
                        <input type="text" id="email" name="email" required class="form-control" disabled value="{{ $pessoa->email }}">
                    </div>
                </div>
                <div class="col-xs-12 col-sm-12 col-md-6">
                    <div class="form-group">
                        <strong>Celular:</strong>
                        <input type="text" id="celular" name="celular" required class="form-control" disabled value="{{ $pessoa->celular }}">
                    </div>
                </div>
                <div class="col-xs-12 col-sm-12 col-md-6">
                    <div class="form-group">
                        <strong>CEP:</strong>
                        <input type="text" id="cep" name="cep" required class="form-control" value="{{$endereco->cep}}" disabled>
                    </div>
                </div>
                <div class="col-md-1"></div>
                <div class="col-xs-12 col-sm-12 col-md-6">
                    <div class="form-group">
                        <strong>Logradouro:</strong>
                        <input type="text" id="logradouro" name="logradouro" required class="form-control"  disabled value="{{ $endereco->logradouro }}">
                    </div>
                </div>
                <div class="col-xs-12 col-sm-12 col-md-2">
                    <div class="form-group">
                        <strong>Número:</strong>
                        <input type="text" id="numero" name="numero" required class="form-control" disabled value="{{ $endereco->numero }}">
                    </div>
                </div>
                <div class="col-xs-12 col-sm-12 col-md-4">
                    <div class="form-group">
                        <strong>Complemento:</strong>
                        <input type="text" id="complemento" name="complemento" required class="form-control" disabled value="{{ $endereco->complemento }}">
                    </div>
                </div>
                <div class="col-xs-12 col-sm-12 col-md-6">
                    <div class="form-group">
                        <strong>Bairro:</strong>
                        <input type="text" id="bairro" name="bairro" required class="form-control" disabled value="{{ $endereco->bairro }}">
                    </div>
                </div>
                <div class="col-xs-12 col-sm-12 col-md-6">
                    <div class="form-group">
                        <strong>Cidade:</strong>
                        <input type="text" name="cidade" id="cidade" required class="form-control" disabled value="{{ $endereco->cidade }}">
                    </div>
                </div>
                <div class="col-xs-12 col-sm-12 col-md-6">
                    <div class="form-group">
                        <strong>Estado:</strong>
                        <input type="text" name="estado" id="estado" required class="form-control" disabled value="{{ $endereco->estado }}">
                    </div>
                </div>
                <div class="col-xs-12 col-sm-12 col-md-6">
                    <div class="form-group">
                        <strong>País:</strong>
                        <input type="text" name="pais" id="pais" required class="form-control" disabled value="{{ $endereco->pais }}">
                    </div>
                </div>
                <div class="col-xs-12 col-sm-12 col-md-6">
                    <div class="form-group">
                        <a class="btn btn-primary" href="{{ route('pacientes.index') }}"> Voltar</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
    $(document).ready(function () {
        
        $('#cpf').mask('999.999.999-99');
        $('#celular').mask('(99)99999-9999');
        $('#cep').mask('99999-999');
       return false;
   });
</script>

@endsection