<!doctype html>
<html>
    <head>
        @include('imports.head')
    </head>
    <body>
        <header class="header" id="top-top">
            @include('imports.header')
        </header>
        
            @yield('content')
        
         
            @include('imports.footer')
            <script>
  function initMap() {
    var uluru = {lat: -22.2903709, lng: -48.5651643};
    var map = new google.maps.Map(document.getElementById('map'), {
      zoom: 18,
      center: uluru
    });
    var marker = new google.maps.Marker({
      position: uluru,
      map: map
    });
  }
</script>
<script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAwrrAVDsoUyGrop91HsCTqAXP31EUo79g&callback=initMap"></script>
    </body>
</html>