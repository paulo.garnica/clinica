<!doctype html>
<html>
  <head>
    @include('imports.head')
  </head>
  <body>
    <header class="header">
      @include('imports.headerApp')
    </header>    
    @yield('content')
  </body>
</html>