@extends('layout.default')
@section('content')

<div class="wrapper">
    <div id="opaco"></div>

    <span class="cuidando fadeInUp-animated fadeInUp animated">Sempre cuidando de <b>você</b> !</span>
    <span class="odonto fadeInUp-animated fadeInUp animated">Odontologia</span>
    <span class="fisio fadeInUp-animated fadeInUp animated">Fisioterapia</span>
    <span class="osteo fadeInUp-animated fadeInUp animated">Osteopatia</span>
    <span class="psico fadeInUp-animated fadeInUp animated">Psicologia</span>
</div>

<!-- <div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">Manage Permission</div>


                <div class="panel-body">


                    @if(checkPermission(['user','admin','superadmin']))
                    <a href="{{ url('permissions-all-users') }}"><button>Access All Users</button></a>
                    @endif


                    @if(checkPermission(['admin','superadmin']))
                    <a href="{{ url('permissions-admin-superadmin') }}"><button>Access Admin and Superadmin</button></a>
                    @endif


                    @if(checkPermission(['superadmin']))
                    <a href="{{ url('permissions-superadmin') }}"><button>Access Only Superadmin</button></a>
                    @endif


                </div>
            </div>
        </div>
    </div>
</div> -->

<!-- container -->
<div class="container pb-1">
    <!-- row -->
    <div class="row fadeInUp-animated text-center" id="1">

        <div class="col-sm-6 fadeInLeft-animated fadeInLeft animated mt-5 mb-3">
            <div class="heading">
                <h3>O melhor para você!</h3>
            </div>
            <!-- <div class="lead mb-1">Contamos com profissionais de várias áreas da saúde para garantir um atendimento
                completo. Agende já sua avaliação!</div> -->
            <p>Atuamos nas seguintes áreas: Cirurgias/Extrações, Clareamento, Clínico Geral,
                Endodontia (Canal), Estética/Dentística, Implantodontia, Implantes, Limpeza/Profilaxia, Prótese,
                Osteopatia, Pilates, Bandagem, Drenagem linfática e Botox. </p>
        </div>
        <div class="col-sm-6 fadeInRight-animated fadeInRight animated mt-5 mb-3">
            <div id="aboutCarousel" class="carousel slide" data-ride="carousel">
                <div class="item active">
                    <div style="background-image: url('../stock2.jpg'); height: 300px; background-position: 50% 0px; border-radius:5px;"
                        data-0="background-position: 50% 0px;" data-500="background-position: 50% -70px;"
                        class="skrollable skrollable-between"></div>
                </div>
            </div>
        </div>
    </div>
    <!-- end row -->

    <!-- row -->
    <div class="row">
        <div class="col-xs-7 col-md-6 mb-3 text-center">
            <a href="#2" class="scroll-down scroll">
                <div class="footer-scrolltop-holder">
                    <span class="ion-ios7-arrow-down footer-scrolltop mt-2 arrow" id="2"></span>
                </div>
            </a>
        </div>
    </div>
    <!-- end row -->
</div>

<div class="mt-2 pb-2" style="background-color: #D6E5F0;">
    <div class="container">
        <div class="row">
            <div class="col-sm-12 fadeInUp-animated text-center fadeInUp animated  mt-4 heading">
                <div class="heading  text-center">
                    <h4><span class="ion-android-social-user mr-3"></span>Quem somos</h4>
                </div>

                <div class="row ">
                    <div class="col-sm-6 fadeInRight-animated fadeInRight animated mt-4 mb-4">
                        <div id="aboutCarousel" class="carousel slide" data-ride="carousel">
                            <div class="item active">
                                <div style="background-image: url('../stock2.jpg'); height: 300px; background-position: 50% 0px; border-radius:5px;"
                                    data-0="background-position: 50% 0px;" data-500="background-position: 50% -70px;"
                                    class="skrollable skrollable-between"></div>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-6 fadeInLeft-animated fadeInLeft animated mt-4 mb-4">
                        <div class="heading">
                            <h3>Dra. Carollinie Knob</h3>
                        </div>
                        <p>Resumo da DRA Carollinie: </p>
                    </div>

                </div>
            </div>
        </div>

        <div class="row heading">
            <div class="col-sm-12 fadeInUp-animated text-center fadeInUp animated  mt-4">

                <div class="row ">
                    <div class="col-sm-6 fadeInLeft-animated fadeInLeft animated mt-4 mb-4">
                        <div class="heading">
                            <h3>Dra. Carollinie Knob</h3>
                        </div>
                        <p>Resumo da DRA Carollinie: </p>
                    </div>
                    <div class="col-sm-6 fadeInRight-animated fadeInRight animated mt-4 mb-4">
                        <div id="aboutCarousel" class="carousel slide" data-ride="carousel">
                            <div class="item active">
                                <div style="background-image: url('../stock2.jpg'); height: 300px; background-position: 50% 0px; border-radius:5px;"
                                    data-0="background-position: 50% 0px;" data-500="background-position: 50% -70px;"
                                    class="skrollable skrollable-between"></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row heading">
            <div class="col-sm-12 fadeInUp-animated text-center fadeInUp animated  mt-4">

                <div class="row ">
                    <div class="col-sm-6 fadeInRight-animated fadeInRight animated mt-4 mb-4">
                        <div id="aboutCarousel" class="carousel slide" data-ride="carousel">
                            <div class="item active">
                                <div style="background-image: url('../stock2.jpg'); height: 300px; background-position: 50% 0px; border-radius:5px;"
                                    data-0="background-position: 50% 0px;" data-500="background-position: 50% -70px;"
                                    class="skrollable skrollable-between"></div>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-6 fadeInLeft-animated fadeInLeft animated mt-4 mb-4">
                        <div class="heading">
                            <h3>Dra. Carollinie Knob</h3>
                        </div>
                        <p>Resumo da DRA Carollinie: </p>
                    </div>

                </div>
            </div>
        </div>
        <!-- row -->
        <div class="row">
            <div class="col-xs-7 col-md-6 mt-2 mb-2 text-center">
                <a href="#3" class="scroll-down scroll">
                    <div class="footer-scrolltop-holder">
                        <span class="ion-ios7-arrow-down footer-scrolltop mt-2 arrow" id="3"></span>
                    </div>
                </a>
            </div>
        </div>
    </div>
</div>
<!-- end row -->
<div class="container">
<div class="row div3" id="3">
    <div class="col-sm-12 fadeInUp-animated text-center fadeInUp animated mt-4">
        <div class="heading  text-center">
            <h4><span class="ion-android-social-user mr15"></span>Localização</h4>
        </div>
        <div class="row ">
            <div class="col-xs-12 col-md-10 col-md-offset-1 m-auto">
                <div id="map" style="min-height:500px;"></div>
            </div>
        </div>

    </div>
</div>
</div>
 
<!-- end localizacao  -->
<div class="container mt-2">
<div class="row div4" id="4">
    <div class="col-sm-12 fadeInUp-animated text-center fadeInUp animated mt-2">
        <div class="heading  text-center">
            <h4><span class="ion-android-social-user mr-2"></span>Contato</h4>
        </div>
        <div id="sucess-form" class="pt-2" hidden>
            <div class="col-md-6 col-md-offset-3">
                <div class="footer-right">
                    <h2 style="text-align: center; color:#000;">Sua mensagem foi enviada!</h2>
                    <p style="color:#000">Entraremos em contato em breve! </p>
                </div>
            </div>
        </div>
        <div id="contact-form">
            <div class="col-md-12">
                <div class="footer-right">
                    <div class="form">
                        <form id="FormContato" action="https://formspree.io/clinicahumaitajau@gmail.com" method="POST">
                            <div class="row">
                                <div class="col-xs-12 col-md-6 col-md-offset-3  m-auto">
                                    <div class="input-group mt10">
                                        <div class="input-group-addon">
                                            <p class="glyphicon glyphicon-user"></p>
                                        </div>
                                        <input type="text" id="name" name="name" class="form-control" value="Nome"
                                            onfocus="this.value = '';"
                                            onblur="if (this.value == '') {this.value = 'Nome';}">
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-xs-12 col-md-6 col-md-offset-3  m-auto">
                                    <div class="input-group mt-1">
                                        <div class="input-group-addon">
                                            <p class="glyphicon glyphicon-envelope"></p>
                                        </div>
                                        <input type="text" id="_replyto" name="_replyto" class="form-control"
                                            value="E-mail" onfocus="this.value = '';"
                                            onblur="if (this.value == '') {this.value = 'E-mail';}">
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-xs-12 col-md-6 col-md-offset-3  m-auto">
                                    <div class="input-group mt-1">
                                        <div class="input-group-addon">
                                            <p class="glyphicon glyphicon-envelope"></p>
                                        </div>
                                        <textarea rows="2" id="message" name="message" class="form-control" cols="70"
                                            onfocus="if(this.value == 'Mensagem') this.value='';"
                                            onblur="if(this.value == '') this.value='Mensagem';"></textarea>
                                    </div>
                                </div>
                            </div>
                            <div class="row mb-2">
                                <div class="col-xs-12 col-md-6 col-md-offset-3 mt10  m-auto">
                                    <input type="submit" class="btn btn-primary submit" value="Enviar"
                                        onclick="Mensagem()">
                                </div>
                            </div>
                            <input type="hidden" name="_next" value="index.html">
                        </form>
                    </div>
                </div>

            </div>
        </div>

    </div>

</div>
</div>

<!-- $(this.hash).offset().top - $("#fixed-navbar").height() -->


@stop