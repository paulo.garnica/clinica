<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Validation Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines contain the default error messages used by
    | the validator class. Some of these rules have multiple versions such
    | as the size rules. Feel free to tweak each of these messages here.
    |
    */

    'accepted' => 'O :attribute deve ser aceito.',
    'active_url' => 'O :attribute não é URL válida.',
    'after' => 'A :attribute deve ser uma data após :date.',
    'after_or_equal' => 'A :attribute deve ser uma data após ou igual a :date.',
    'alpha' => 'O :attribute deve conter apenas letras.',
    'alpha_dash' => 'O :attribute deve conter apenas letras, números, traços e sublinhado.',
    'alpha_num' => 'O :attribute deve conter apenas letras, números.',
    'array' => 'O :attribute deve ser um array.',
    'before' => 'A :attribute deve ser uma data antes :date.',
    'before_or_equal' => 'A :attribute deve ser uma data antes ou igual a to :date.',
    'between' => [
        'numeric' => 'O :attribute deve estar entre :min e :max.',
        'file' => 'The :attribute deve estar entre :min e :max kilobytes.',
        'string' => 'The :attribute deve estar entre :min e :max caracteres.',
        'array' => 'a :attribute deve estar entre :min e :max items.',
    ],
    'boolean' => 'O :attribute deve ser verdadeiro ou falso.',
    'confirmed' => 'Os :attribute não são iguais.',
    'date' => 'A :attribute não é uma data válida.',
    'date_equals' => 'A :attribute deve ser igual a :date.',
    'date_format' => 'a :attribute não esta no formato recomendado :format.',
    'different' => 'O :attribute e :other devem ser diferentes.',
    'digits' => 'O :attribute deve ter :digits dígitos.',
    'digits_between' => 'O :attribute deve ter entre :min e :max dígitos.',
    'dimensions' => 'A :attribute tem dimensões inválidas.',
    'distinct' => 'O :attribute tem valor duplicado.',
    'email' => 'O :attribute deve ser um e-mail válido.',
    'ends_with' => 'O :attribute deve terminar com: :values.',
    'exists' => 'O :attribute selecionado é inválido.',
    'file' => 'O :attribute deve ser um arquivo.',
    'filled' => 'O :attribute deve conter um valor.',
    'gt' => [
        'numeric' => 'O :attribute deve ser maior que :value.',
        'file' => 'O :attribute deve conter mais que :value kilobytes.',
        'string' => 'O :attribute deve conter mais que :value caracteres.',
        'array' => 'A :attribute deve conter mais que :value items.',
    ],
    'gte' => [
        'numeric' => 'O :attribute deve ser maior ou igual que :value.',
        'file' => 'O :attribute deve ser maior ou igual que :value kilobytes.',
        'string' => 'O :attribute deve ser maior ou igual que :value caracteres.',
        'array' => 'a :attribute deve ter :value items ou mais.',
    ],
    'image' => 'O :attribute deve ser uma imagem.',
    'in' => 'O :attribute selecionado é inválido.',
    'in_array' => 'O :attribute não existe em :other.',
    'integer' => 'O :attribute deve ser um inteiro.',
    'ip' => 'O :attribute deve ser um IP válido.',
    'ipv4' => 'O :attribute deve ser um IPv4 válido.',
    'ipv6' => 'O :attribute deve ser um IPv6 válido.',
    'json' => 'O :attribute deve ser um JSON válido.',
    'lt' => [
        'numeric' => 'O :attribute deve ser menor que :value.',
        'file' => 'O :attribute deve ser menor que :value kilobytes.',
        'string' => 'O :attribute deve ser menor que :value caracteres.',
        'array' => 'A :attribute deve ser menor que :value items.',
    ],
    'lte' => [
        'numeric' => 'O :attribute deve ser menor ou igual que :value.',
        'file' => 'O :attribute deve ser menor ou igual :value kilobytes.',
        'string' => 'O :attribute deve ser menor ou igual :value caracteres.',
        'array' => 'A :attribute não deve conter mais que :value items.',
    ],
    'max' => [
        'numeric' => 'O :attribute não deve ser maior que :max.',
        'file' => 'O :attribute não deve ser maior que :max kilobytes.',
        'string' => 'O :attribute não deve ser maior que :max caracteres.',
        'array' => 'A :attribute não deve ser maior que :max items.',
    ],
    'mimes' => 'O :attribute deve ser um arquivo do tipo: :values.',
    'mimetypes' => 'O :attribute deve ser um arquivo do tipo: :values.',
    'min' => [
        'numeric' => 'O :attribute deve conter ao menos :min.',
        'file' => 'O :attribute deve conter ao menos :min kilobytes.',
        'string' => 'O :attribute deve conter ao menos :min caracteres.',
        'array' => 'A :attribute deve conter ao menos :min items.',
    ],
    'not_in' => 'O :attribute selecionado é inválido.',
    'not_regex' => 'O formato do :attribute é inválido.',
    'numeric' => 'O :attribute deve ser numérico.',
    'password' => 'A senha é inválida.',
    'present' => 'O :attribute deve ser preenchido.',
    'regex' => 'O formato do :attribute é inválido.',
    'required' => 'O :attribute é obrigatório.',
    'required_if' => 'O :attribute é obrigatório quando :other é :value.',
    'required_unless' => 'O :attribute é obrigatório ao menos que :other é :values.',
    'required_with' => 'O campo :attribute é obrigatório quando :values é fornecido.',
    'required_with_all' => 'O :attribute é obrigatório quando :values são fornecidos.',
    'required_without' => 'O :attribute é obrigatório quando :values não é fornecido.',
    'required_without_all' => 'O :attribute é obrigatório quando :values não são fornecido.',
    'same' => 'O campo :attribute e :other devem ser iguais.',
    'size' => [
        'numeric' => 'O :attribute deve ter tamanho de :size.',
        'file' => 'O :attribute deve ter tamanho de :size kilobytes.',
        'string' => 'O :attribute deve ter tamanho de :size caracteres.',
        'array' => 'A :attribute deve ter tamanho de  :size items.',
    ],
    'starts_with' => 'O :attribute deve começar com: :values.',
    'string' => 'O :attribute deve ser uma string.',
    'timezone' => 'O :attribute deve ser um fuso válido.',
    'unique' => 'O :attribute deve ser único.',
    'uploaded' => 'O :attribute falho ao ser enviado.',
    'url' => 'O :attribute é inválido.',
    'uuid' => 'O :attribute deve ser UUID válido.',

    /*
    |--------------------------------------------------------------------------
    | Custom Validation Language Lines
    |--------------------------------------------------------------------------
    |
    | Here you may specify custom validation messages for attributes using the
    | convention "attribute.rule" to name the lines. This makes it quick to
    | specify a specific custom language line for a given attribute rule.
    |
    */

    'custom' => [
        'attribute-name' => [
            'rule-name' => 'custom-message',
        ],
    ],

    /*
    |--------------------------------------------------------------------------
    | Custom Validation Attributes
    |--------------------------------------------------------------------------
    |
    | The following language lines are used to swap our attribute placeholder
    | with something more reader friendly such as "E-Mail Address" instead
    | of "email". This simply helps us make our message more expressive.
    |
    */

    'attributes' => [],

];
